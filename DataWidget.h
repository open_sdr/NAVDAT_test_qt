#ifndef DATAWIDGET_H
#define DATAWIDGET_H

#include <QWidget>

#include <sdr_math.h>
#include "NAVDAT/Modem/common.h"

namespace Ui {
class DataWidget;
}

class DataWidget : public QWidget
{
  Q_OBJECT

public:
  explicit DataWidget(QWidget *parent = 0);
  ~DataWidget();

  void* Test;

  bool isAutoTransmit();
  bool isShift();
  bool isRandShift();
  int Shift();
  QString SenderName();
  QString FileName();
  int DataCount();
  NAVDAT_TIS_qamMode_t tisQAM();
  NAVDAT_DS_qamMode_t dsQAM();

  double SiganalGain(){return from_dB(SiganalGain_dB());}
  double SiganalGain_dB();

  double NoiseLevel_dB();

  bool isSyncWindowOffset();
  int SyncWindowOffset();
  void setSymbolIdx(quint32 idx);
  void setSenderName2(QString Name);
  void setFileName2(QString Name);
  void setDataCount2(int Count);
  void setTisQAM(NAVDAT_TIS_qamMode_t mode);
  void setDsQAM(NAVDAT_DS_qamMode_t mode);

  void setErrorsCounts(NAVDAT_DataCounter_t *qamErr, NAVDAT_DataCounter_t *frameErr);

private slots:
  void on_Transmit_clicked();

  void on_isAutoTransmit_clicked();

private:
  Ui::DataWidget *ui;
};

#endif // DATAWIDGET_H
