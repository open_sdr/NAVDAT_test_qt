#ifndef MODULATORTEST_H
#define MODULATORTEST_H

#include <TestApp.h>
#include <TestControl.h>
#include "DataWidget.h"

#include <SDR/BASE/Multiplex/symMultiplex.h>
#include <SDR/BASE/Noises/awgn.h>

#include <NAVDAT/Multiplex/Multiplex.h>
#include <NAVDAT/Modem/Modulator.h>
#include <NAVDAT/Modem/Demodulator.h>
#include <NAVDAT/Synchronization/detection.h>
#include <NAVDAT/Synchronization/MLE.h>

#include <SDR/Qt_Addons/TScopePlot.h>
#include <SDR/Qt_Addons/TBufferPlot.h>
#include <SDR/Qt_Addons/TSpectrumPlot.h>
#include <SDR/Qt_Addons/TScatterPlot.h>

#include <QVector>

using namespace SDR;

class ModulationTest : public TestApp
{
public:
  ModulationTest();

  void ifft_samples_handler(iqSample_t* Samples, Size_t Count);
  void data_sent_handler();

  Bool_t sync_detector(iqSample_t *Samples, Size_t Count);
  void sync_symbol_handler(iqSample_t *Samples, Size_t Count);
  void fft_samples_handler(iqSample_t *Samples, Size_t Count);
  void signal_detected_handler();
  void signal_not_detected_handler();
  void gain_factors_handler();
  void phase_offset_handler();
  void collector_ready_handler();
  void collector_processed_handler();
  void qam_symbol_handler(iqSample_t Sample);
  void mis_ready_handler(qamBuffer_t* buf);
  void tis_ready_handler(qamBuffer_t* buf);
  void ds_ready_handler(qamBuffer_t* buf);

  void transmit();

private:
  QByteArray SenderID, FileName;

  Frequency_t Fd;
  Size_t fftSize;
  double dt, St;

  Value_t syncWindowOffset;

  Noise_Awgn_t Awgn;
  Noise_AwgnCfg_t cfgAwgn;

  NAVDAT_Multiplex_t Mux, deMux;
  NAVDAT_MultiplexCfg_t cfgMux;

  NAVDAT_Modulator_t Mod;
  NAVDAT_ModulatorCfg_t cfgMod;
  NAVDAT_qamData_t qamDataNew, qamData1;

  NAVDAT_SignalDetector_t Detector;
  NAVDAT_SignalDetectorCfg_t cfgDetector;
  NAVDAT_mleSync_t sync;
  NAVDAT_mleSyncCfg_t cfgSync;
  NAVDAT_Demodulator_t deMod;
  NAVDAT_DemodulatorCfg_t cfgDeMod;
  NAVDAT_qamData_t qamData2;

  NAVDAT_Frame_t FrameNew, Frame1, Frame2;
  NAVDAT_RawFrame_t rawFrameNew, rawFrame1, rawFrame2;

  char frame2_FileNameBuf[255+1];

  QVector<UInt8_t> Data, Data2;
  QVector<qamSymbol_t> misDataBuf, tisDataBuf, dsDataBuf;
  QVector<qamSymbol_t> misData1Buf, tisData1Buf, dsData1Buf;
  QVector<qamSymbol_t> misData2Buf, tisData2Buf, dsData2Buf;

  QVector<iqSample_t> ChannelBuf, syncBuf, CollectorBuf, qamSymbols;
  QVector<Sample_t> syncCorrelationBuf, syncFreqEstimationBuf, syncWindowIBuf, DetectorBuf, deModRecoveryABuf, deModRecoveryPhBuf;

  TScopePlot Scope;
  TSpectrumPlot fftModPlot, fftDemPlot;
  DataWidget WData;

  TBufferPlot syncWindowPlot, syncCorrealtionPlot, syncFreqEstimationPlot, equalizerPlot, dPhasePlot;
  QCPItemStraightLine *syncWindow_SymbolStart, *syncWindow_SymbolStop, *syncWindow_offsetIdx;
  TScatterPlot scatterPlot;

  NAVDAT_DataCounter_t qamDataErr, rawFrameErr;

  void prestart();
  void reset();
  void test();
};

#endif // MODULATORTEST_H
