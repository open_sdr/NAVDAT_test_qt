NAVDAT_PATH = $$PWD/NAVDAT
unix {
    NAVDAT_HEADERS += $$system(find $$NAVDAT_PATH -name \*.h\*)
    for(file, NAVDAT_HEADERS) HEADERS += $$file

    NAVDAT_SOURCES += $$system(find $$NAVDAT_PATH -name \*.c\*)
    for(file, NAVDAT_SOURCES) SOURCES += $$file
}


