#include "ModulationTest.h"
#include "sdr_complex.h"
#include "sdr_math.h"
#include "NAVDAT/Modem/ofdmMaps.h"
#include "NAVDAT/Codec/Codec.h"

extern "C"
{
  static void fill_empty_buf(ModulationTest* This, iqSample_t* Samples, Size_t Count)
  {
    Q_UNUSED(This);
    clear_buffer_iq(Samples, Count);
  }
  static void ifft_samples_sink(ModulationTest* This, iqSample_t* Samples, Size_t Count)
  {
    This->ifft_samples_handler(Samples, Count);
  }
  static void data_sent(ModulationTest* This)
  {
    This->data_sent_handler();
  }

  Bool_t sync_detector_sink (ModulationTest* This, iqSample_t* Samples, Size_t Count)
  {
    return This->sync_detector(Samples, Count);
  }

  static void sync_symbol_sink(ModulationTest* This, iqSample_t* Samples, Size_t Count)
  {
    This->sync_symbol_handler(Samples, Count);
  }

  static void fft_samples_sink(ModulationTest* This, iqSample_t* Samples, Size_t Count)
  {
    This->fft_samples_handler(Samples, Count);
  }

  static void demodulator_on_signal_detected(ModulationTest* This)
  {
    This->signal_detected_handler();
  }

  static void demodulator_on_signal_not_detected(ModulationTest* This)
  {
    This->signal_not_detected_handler();
  }

  static void demodulator_on_da_ready(ModulationTest* This)
  {
    This->gain_factors_handler();
  }

  static void demodulator_on_dph_ready(ModulationTest* This)
  {
    This->phase_offset_handler();
  }

  static void demodulator_on_collector_ready(ModulationTest* This)
  {
    This->collector_ready_handler();
  }

  static void demodulator_on_collector_processed(ModulationTest* This)
  {
    This->collector_processed_handler();
  }

  static void demodulator_on_qam_symbol(ModulationTest* This, iqSample_t Sample)
  {
    This->qam_symbol_handler(Sample);
  }

  static void demodulator_on_mis_ready(ModulationTest* This, qamBuffer_t* buf)
  {
    This->mis_ready_handler(buf);
  }

  static void demodulator_on_tis_ready(ModulationTest* This, qamBuffer_t* buf)
  {
    This->tis_ready_handler(buf);
  }

  static void demodulator_on_ds_ready(ModulationTest* This, qamBuffer_t* buf)
  {
    This->ds_ready_handler(buf);
  }
}

static Size_t cmp_uint8buf(UInt8_t* buf1, UInt8_t* buf2, Size_t n)
{
  Size_t err = 0;

  for (Size_t i = 0; i < n; i++)
    if (buf1[i] != buf2[i])
      err++;
  return err;
}

static Size_t cmp_qambuf(qamBuffer_t *buf1, qamBuffer_t *buf2)
{
  Size_t n = (buf1->Size <= buf2->Size)? buf1->Size : buf2->Size;
  return cmp_uint8buf((UInt8_t*)buf1->P, (UInt8_t*)buf2->P, n);
}

/* idx: 0 -- mis; 1 -- tis; 2 -- ds */
static Size_t cmp_frame(int idx, NAVDAT_RawFrame_t *frame1, NAVDAT_RawFrame_t *frame2)
{
  UInt8_t *buf1, *buf2;
  Size_t n;
  switch (idx)
  {
  case 0:
    buf1 = frame1->misData;
    buf2 = frame2->misData;
    n = (frame1->Count.mis <= frame2->Count.mis)? frame1->Count.mis : frame2->Count.mis;
    break;

  case 1:
    buf1 = frame1->tisData;
    buf2 = frame2->tisData;
    n = (frame1->Count.tis <= frame2->Count.tis)? frame1->Count.tis : frame2->Count.tis;
    break;

  case 2:
    buf1 = frame1->dsData;
    buf2 = frame2->dsData;
    n = (frame1->Count.ds <= frame2->Count.ds)? frame1->Count.ds : frame2->Count.ds;
    break;
  }
  return cmp_uint8buf(buf1, buf2, n);
}

ModulationTest::ModulationTest() :
  TestApp(500), Scope(TScopePlot::mComplex), fftModPlot(0, 0), fftDemPlot(0, 0)
{
  WData.Test = this;

  /* Настройки */

  cfgMod.fftSizeFactor = 1;
  cfgAwgn.SignalNoiseRatio = 80;

  Fd = cfgMod.fftSizeFactor*12000;
  St = 1.0/Fd; // SampleTime
  fftSize = NAVDAT_Nfft(cfgMod.fftSizeFactor);

  cfgSync.N = NAVDAT_Nfft(cfgMod.fftSizeFactor);
  cfgSync.L = NAVDAT_OFDM_GUARD_INTERVAL_SAMPLES_COUNT(cfgMod.fftSizeFactor);

  /* Буфера */

  ChannelBuf.resize(NAVDAT_OFDM_SYMBOL_SAMPLES_COUNT(cfgMod.fftSizeFactor));

  Data.resize(NAVDAT_dsRawCountMax(NAVDAT_DS_qam64));

  misDataBuf.resize(NAVDAT_MIS_CELLS_COUNT);
  tisDataBuf.resize(NAVDAT_TIS_CELLS_COUNT);
  dsDataBuf.resize(NAVDAT_DS_CELLS_COUNT);

  misData1Buf.resize(NAVDAT_MIS_CELLS_COUNT);
  tisData1Buf.resize(NAVDAT_TIS_CELLS_COUNT);
  dsData1Buf.resize(NAVDAT_DS_CELLS_COUNT);

  syncBuf.resize(NAVDAT_mleSYNC_BUFSIZE(cfgSync.N, cfgSync.L));
  syncWindowIBuf.resize(syncBuf.size());
  syncCorrelationBuf.resize(NAVDAT_mleSYNC_ESTIMATION_BUFSIZE(cfgSync.N, cfgSync.L));
  syncFreqEstimationBuf.resize(NAVDAT_mleSYNC_ESTIMATION_BUFSIZE(cfgSync.N, cfgSync.L));
  DetectorBuf.resize(fftSize);
  CollectorBuf.resize(NAVDAT_DEMODULATOR_COLLECTOR_SIZE(cfgMod.fftSizeFactor));
  deModRecoveryABuf.resize(NAVDAT_OFDM_SYMBOL_CELLS_COUNT);
  deModRecoveryPhBuf.resize(NAVDAT_OFDM_SYMBOL_CELLS_COUNT);

  misData2Buf.resize(NAVDAT_MIS_CELLS_COUNT);
  tisData2Buf.resize(NAVDAT_TIS_CELLS_COUNT);
  dsData2Buf.resize(NAVDAT_DS_CELLS_COUNT);

  Data2.resize(NAVDAT_dsRawCountMax(NAVDAT_DS_qam64));

  /* Интерфейсы к буферам */

  FrameNew.data.P = Data.data();

  qamDataNew.mis.P = misDataBuf.data();
  qamDataNew.mis.Size = misDataBuf.size();
  qamDataNew.tis.P = tisDataBuf.data();
  qamDataNew.tis.Size = tisDataBuf.size();
  qamDataNew.ds.P = dsDataBuf.data();
  qamDataNew.ds.Size = dsDataBuf.size();

  qamData1.mis.P = misData1Buf.data();
  qamData1.mis.Size = misData1Buf.size();
  qamData1.tis.P = tisData1Buf.data();
  qamData1.tis.Size = tisData1Buf.size();
  qamData1.ds.P = dsData1Buf.data();
  qamData1.ds.Size = dsData1Buf.size();

  qamData2.mis.P = misData2Buf.data();
  qamData2.mis.Size = misData2Buf.size();
  qamData2.tis.P = tisData2Buf.data();
  qamData2.tis.Size = tisData2Buf.size();
  qamData2.ds.P = dsData2Buf.data();
  qamData2.ds.Size = dsData2Buf.size();

  Frame2.FileName = frame2_FileNameBuf;
  Frame2.data.P = Data2.data();

  /* Инициализация */

  cfgMux.modeTIS = NAVDAT_TIS_qam4;
  cfgMux.modeDS = NAVDAT_DS_qam4;
  cfgMux.codeXIS = NAVDAT_xIS_Code_None;
  cfgMux.codeDS = NAVDAT_DS_Code_None;

  cfgMod.fftAmplitudeFactor = 1;
  cfgMod.qamTIS = cfgMux.modeTIS;
  cfgMod.qamDS = cfgMux.modeDS;
  init_NAVDAT_Modulator(&Mod, &cfgMod);

  cfgDetector.Treshold = 0.2;
  cfgDetector.Step = fftSize;
  cfgDetector.fftBuf.P = CollectorBuf.data();
  cfgDetector.fftBuf.Size = fftSize;
  cfgDetector.magBuf.P = (Sample_t*)CollectorBuf.data()+2*fftSize;
  cfgDetector.magBuf.Size = fftSize;
  init_NAVDAT_SignalDetector(&Detector, &cfgDetector);

  cfgSync.SNR = cfgAwgn.SignalNoiseRatio;
  cfgSync.MaxOffsetInSync = 125;
  cfgSync.Buf.P = syncBuf.data();
  cfgSync.Buf.Size = syncBuf.size();
  cfgSync.CorrelationBuf.P = syncCorrelationBuf.data();
  cfgSync.CorrelationBuf.Size = syncCorrelationBuf.size();
  cfgSync.FreqEstimationBuf.P = syncFreqEstimationBuf.data();
  cfgSync.FreqEstimationBuf.Size = syncFreqEstimationBuf.size();
  init_NAVDAT_mleSync(&sync, &cfgSync);

  cfgDeMod.fftSizeFactor = cfgMod.fftSizeFactor;
  cfgDeMod.Data = &qamData2;
  cfgDeMod.DetectorTreshold = 0.5;
  cfgDeMod.DetectorBuf.P = DetectorBuf.data();
  cfgDeMod.DetectorBuf.Size = DetectorBuf.size();
  cfgDeMod.Collector.P = CollectorBuf.data();
  cfgDeMod.Collector.Size = CollectorBuf.size();
  cfgDeMod.aBuf.P = deModRecoveryABuf.data();
  cfgDeMod.aBuf.Size = deModRecoveryABuf.size();
  cfgDeMod.phBuf.P = deModRecoveryPhBuf.data();
  cfgDeMod.phBuf.Size = deModRecoveryPhBuf.size();
  init_NAVDAT_Demodulator(&deMod, &cfgDeMod);

  NAVDAT_Multiplex_reset(&deMux);

  cfgAwgn.SignalLevel = 1;
  init_Noise_Awgn(&Awgn, &cfgAwgn);

  /* Обратные вызовы */

  NAVDAT_ModulatorCallback_t callbackMod;
  callbackMod.on_data_sent = (event_sink_t)data_sent;
  callbackMod.fill_empty_buf = (samples_sink_iq_t)fill_empty_buf;
  callbackMod.ifft_samples_sink = (samples_sink_iq_t)ifft_samples_sink;
  NAVDAT_Modulator_set_callback(&Mod, this, &callbackMod);

  NAVDAT_mleSyncCallback_t callbackSync;
  callbackSync.detect_signal = (samples_solver_iq_t)sync_detector_sink;
  callbackSync.on_symbol_ready = (samples_sink_iq_t)sync_symbol_sink;
  NAVDAT_mleSync_setCallback(&sync, this, &callbackSync);

  NAVDAT_DemodulatorCallback_t callbackDem;
  callbackDem.fft_samples_sink = (samples_sink_iq_t)fft_samples_sink;
  callbackDem.on_signal_detected = (event_sink_t)demodulator_on_signal_detected;
  callbackDem.on_signal_not_detected = (event_sink_t)demodulator_on_signal_not_detected;
  callbackDem.on_dA_ready = (event_sink_t)demodulator_on_da_ready;
  callbackDem.on_dPh_ready = (event_sink_t)demodulator_on_dph_ready;
  callbackDem.on_mis_ready = (qambuffer_sink_t)demodulator_on_mis_ready;
  callbackDem.on_tis_ready = (qambuffer_sink_t)demodulator_on_tis_ready;
  callbackDem.on_ds_ready = (qambuffer_sink_t)demodulator_on_ds_ready;
  callbackDem.on_collector_ready = (event_sink_t)demodulator_on_collector_ready;
  callbackDem.on_collector_processed = (event_sink_t)demodulator_on_collector_processed;
  callbackDem.on_qam_symbol = (sample_sink_iq_t)demodulator_on_qam_symbol;
  NAVDAT_Demodulator_set_callback(&deMod, this, &callbackDem);

  /* Виджеты */

  fftModPlot.setFd(Fd);
  fftModPlot.setNfft(fftSize);
  fftModPlot.setAutoRangeY(true);

  syncCorrealtionPlot.setAutoRangeY(true);
  syncFreqEstimationPlot.setAutoRangeY(true);

  syncWindowPlot.setAutoRangeY(true);
  syncWindowPlot.setAutoReplot(false);
  // Начало символа
  syncWindow_SymbolStart = new QCPItemStraightLine(syncWindowPlot.getCustomPlot());
  syncWindow_SymbolStart->setPen(QPen(Qt::green));
  syncWindow_SymbolStart->point1->setCoords(sync.currentSymbolOffset+sync.L, 0);
  syncWindow_SymbolStart->point2->setCoords(sync.currentSymbolOffset+sync.L, 1);
  // Конец символа
  syncWindow_SymbolStop = new QCPItemStraightLine(syncWindowPlot.getCustomPlot());
  syncWindow_SymbolStop->setPen(QPen(Qt::green));
  syncWindow_SymbolStop->point1->setCoords(sync.currentSymbolOffset+sync.L+sync.N-1, 0);
  syncWindow_SymbolStop->point2->setCoords(sync.currentSymbolOffset+sync.L+sync.N-1, 1);
  // Сдвиг
  syncWindow_offsetIdx = new QCPItemStraightLine(syncWindowPlot.getCustomPlot());
  syncWindow_offsetIdx->setPen(QPen(Qt::red));
  syncWindow_offsetIdx->point1->setCoords(sync.currentSymbolOffset, 0);
  syncWindow_offsetIdx->point2->setCoords(sync.currentSymbolOffset, 1);

  equalizerPlot.setRangeY(0, 4);

  dPhasePlot.setRangeY(-SDR_PI, SDR_PI);

  scatterPlot.setRangeX(-8, 8);
  scatterPlot.setRangeY(-8, 8);

  fftDemPlot.setFd(Fd);
  fftDemPlot.setNfft(fftSize);
  fftDemPlot.setAutoRangeY(true);

  double Ts = (double)NAVDAT_Ts_ms/1000;
  Scope.setCollectData(true);
  Scope.setRefreshInterval(Ts);
  Scope.setTimeInterval(17*Ts);
  Scope.setAutoRangeY(true);

  TPlotsWidget *plotsWidget = Control()->Plots(), *plotsWidget2;
  //plotsWidget->addPlot(&fftModPlot);
  plotsWidget->addPlot(&Scope);

  plotsWidget = new TPlotsWidget(0, QBoxLayout::LeftToRight);
  plotsWidget->addPlot(&syncCorrealtionPlot);
  plotsWidget->addPlot(&syncFreqEstimationPlot);
  plotsWidget->addPlot(&syncWindowPlot);
  Control()->Plots()->addPlotsWidget(plotsWidget);  

  plotsWidget = new TPlotsWidget(0, QBoxLayout::LeftToRight);
  plotsWidget->setPlotMinimumSize(220, 200);
  plotsWidget->addPlot(&fftDemPlot, 3);
  plotsWidget2 = new TPlotsWidget(0, QBoxLayout::TopToBottom);
  plotsWidget2->setPlotMinimumSize(220, 100);
  plotsWidget2->addPlot(&equalizerPlot);
  plotsWidget2->addPlot(&dPhasePlot);
  plotsWidget->addPlotsWidget(plotsWidget2, 3);
  plotsWidget->addPlot(&scatterPlot, 1);
  Control()->Plots()->addPlotsWidget(plotsWidget);

  Control()->appendUserWidget(&WData);
}

void copy_qam_data(NAVDAT_qamData_t* src, NAVDAT_qamData_t* dst)
{
  dst->tisMode = src->tisMode;
  dst->dsMode = src->dsMode;
  COPY_BUFFER(qamSymbol_t, src->mis.P, src->mis.Size, dst->mis.P);dst->mis.Size=src->mis.Size;
  COPY_BUFFER(qamSymbol_t, src->tis.P, src->tis.Size, dst->tis.P);dst->tis.Size=src->tis.Size;
  COPY_BUFFER(qamSymbol_t, src->ds.P, src->ds.Size, dst->ds.P);dst->ds.Size=src->ds.Size;
}

void ModulationTest::data_sent_handler()
{
  Frame1 = FrameNew;
  rawFrame1 = rawFrameNew;

  copy_qam_data(&qamDataNew, &qamData1);

  if (WData.isAutoTransmit())
    transmit();
}

void ModulationTest::ifft_samples_handler(iqSample_t *Samples, Size_t Count)
{
  fftModPlot.append_fft(Samples, Count);
}

Bool_t ModulationTest::sync_detector(iqSample_t* Samples, Size_t Count)
{
#if 1
  Bool_t res = navdat_signal_detector(&Detector, Samples, Count);
  fftDemPlot.append_fft(Detector.fftBuf.P, fftSize);
  return res;
#else
  return true;
#endif
}

void ModulationTest::sync_symbol_handler(iqSample_t *Samples, Size_t Count)
{
  syncCorrealtionPlot.append(syncCorrelationBuf.data(), syncCorrelationBuf.size());
  syncFreqEstimationPlot.append(syncFreqEstimationBuf.data(), syncFreqEstimationBuf.size());

  if (WData.isSyncWindowOffset())
  {
    syncWindowOffset = WData.SyncWindowOffset();
    if (((sync.currentSymbolOffset == 0)&&(syncWindowOffset < 0))||(sync.currentSymbolOffset + syncWindowOffset >= sync.N+sync.L))
      syncWindowOffset = 0;
    sync.currentSymbolOffset += syncWindowOffset;
  }
  else
    syncWindowOffset = 0;

  WData.setSymbolIdx(sync.currentSymbolOffset);

  iq_demux_i(sync.syncWindow.Window.P, sync.syncWindow.Window.Size, syncWindowIBuf.data());
  syncWindowPlot.append(syncWindowIBuf.data(), syncWindowIBuf.size());
  syncWindow_offsetIdx->point1->setCoords(sync.currentSymbolOffset, 0);
  syncWindow_offsetIdx->point2->setCoords(sync.currentSymbolOffset, 1);
  syncWindow_SymbolStart->point1->setCoords(sync.currentSymbolOffset+sync.L, 0);
  syncWindow_SymbolStart->point2->setCoords(sync.currentSymbolOffset+sync.L, 1);
  syncWindow_SymbolStop->point1->setCoords(sync.currentSymbolOffset+sync.L+sync.N-1, 0);
  syncWindow_SymbolStop->point2->setCoords(sync.currentSymbolOffset+sync.L+sync.N-1, 1);
  syncWindowPlot.replot();

  navdat_demodulator_iq(&deMod, Samples+syncWindowOffset, Count);
}

void ModulationTest::fft_samples_handler(iqSample_t *Samples, Size_t Count)
{
  fftDemPlot.append_fft(Samples, Count);
}

void ModulationTest::signal_detected_handler()
{
  NAVDAT_mleSync_enable(&sync);
}

void ModulationTest::signal_not_detected_handler()
{
  NAVDAT_mleSync_disable(&sync);
}

void ModulationTest::gain_factors_handler()
{
  equalizerPlot.append(deModRecoveryABuf.data(), deModRecoveryABuf.size());
}

void ModulationTest::phase_offset_handler()
{
  dPhasePlot.append(deModRecoveryPhBuf.data(), deModRecoveryPhBuf.size());
}

void ModulationTest::collector_ready_handler()
{
  qamSymbols.clear();
}

void ModulationTest::collector_processed_handler()
{
  scatterPlot.append(qamSymbols.data(), qamSymbols.size());
}

void ModulationTest::qam_symbol_handler(iqSample_t Sample)
{
  qamSymbols.append(Sample);
}

void ModulationTest::mis_ready_handler(qamBuffer_t *buf)
{
  Bool_t res = 0;

  qamDataErr.mis = cmp_qambuf(&qamData1.mis, buf);

  res = NAVDAT_misBufToFrame(buf, &rawFrame2);
  if(!res) qWarning("Convert TIS failure");

  rawFrameErr.mis = cmp_frame(0, &rawFrame1, &rawFrame2);

  NAVDAT_Multiplex_reset(&deMux);
  res = navdat_multiplex_read_mis(&deMux, &rawFrame2);
  if(!res) qWarning("Read MIS failure");

  NAVDAT_Demodulator_set_tisMode(&deMod, NAVDAT_Multiplex_tisMode(&deMux));
  NAVDAT_Demodulator_set_dsMode(&deMod, NAVDAT_Multiplex_dsMode(&deMux));

  WData.setTisQAM(NAVDAT_Multiplex_tisMode(&deMux));
  WData.setDsQAM(NAVDAT_Multiplex_dsMode(&deMux));

  WData.setErrorsCounts(&qamDataErr, &rawFrameErr);
}

void ModulationTest::tis_ready_handler(qamBuffer_t *buf)
{
  Bool_t res = 0;

  qamDataErr.tis = cmp_qambuf(&qamData1.tis, buf);

  res = NAVDAT_tisBufToFrame(NAVDAT_Multiplex_tisMode(&deMux), buf, &rawFrame2);
  if(!res) qWarning("Convert TIS failure");

  rawFrameErr.tis = cmp_frame(1, &rawFrame1, &rawFrame2);

  res = navdat_multiplex_read_tis(&deMux, &rawFrame2, &Frame2);
  if(!res) qWarning("Read TIS failure");

  WData.setErrorsCounts(&qamDataErr, &rawFrameErr);

  WData.setSenderName2(QString::fromLocal8Bit(deMux.SenderID));
}

void ModulationTest::ds_ready_handler(qamBuffer_t *buf)
{
  Bool_t res = 0;

  qamDataErr.ds = cmp_qambuf(&qamData1.ds, buf);

  res = NAVDAT_dsBufToFrame(NAVDAT_Multiplex_dsMode(&deMux), buf, &rawFrame2);
  if(!res) qWarning("Convet DS failure");

  rawFrameErr.ds = cmp_frame(2, &rawFrame1, &rawFrame2);

  res = navdat_multiplex_read_ds(&deMux, &rawFrame2, &Frame2);
  if(!res) qWarning("Read TIS failure");

  WData.setErrorsCounts(&qamDataErr, &rawFrameErr);

  WData.setFileName2(QString::fromLocal8Bit(Frame2.FileName));
  WData.setDataCount2(Frame2.data.Size);
}

void ModulationTest::transmit()
{
  if (NAVDAT_Modulator_isTransmit(&Mod)) return;

  NAVDAT_DataCounter_reset(&qamDataErr);
  NAVDAT_DataCounter_reset(&rawFrameErr);

  /* Заполнение данных */

  SenderID = WData.SenderName().toLocal8Bit();
  FileName = WData.FileName().toLocal8Bit();

  FrameNew.idFile = 0;
  FrameNew.FileName = FileName.data();
  FrameNew.id = 0;
  FrameNew.flags = 3;
  FrameNew.timestamp = rand() & 0x7fff;

  cfgMux.modeTIS = WData.tisQAM();
  cfgMux.modeDS = WData.dsQAM();
  init_NAVDAT_Multiplex(&Mux, &cfgMux);

  Size_t size;
  if (WData.DataCount() == -1)
    size = NAVDAT_Multiplex_dsDataCountMax(&Mux);
  else
    size = WData.DataCount();
  for (Size_t i = 0; i < size; i++)
    Data[i] = rand() % 256;
  FrameNew.data.Size = size;

  NAVDAT_Multiplex_setSenderID(&Mux, SenderID.data());
  NAVDAT_Multiplex_setDate(&Mux, 89, 12, 11);
  NAVDAT_Multiplex_setTime(&Mux, 11, 11, 11);

  NAVDAT_RawFrame_t rawFrame;
  navdat_multiplex_write(&Mux, &FrameNew, &rawFrame);

  NAVDAT_CodecCfg_t cfgCod;
  cfgCod.tisMode = cfgMux.modeTIS;
  cfgCod.dsMode = cfgMux.modeDS;
  cfgCod.xisCode = cfgMux.codeXIS;
  cfgCod.dsCode = cfgMux.codeDS;
  NAVDAT_Codec_t Cod;
  init_NAVDAT_Codec(&Cod, &cfgCod);

  navdat_encode(&Cod, &rawFrame, &rawFrameNew);

  deinit_NAVDAT_Codec(&Cod);

  qamDataNew.mis.Size = misDataBuf.size();
  qamDataNew.tis.Size = tisDataBuf.size();
  qamDataNew.ds.Size = dsDataBuf.size();
  NAVDAT_FrameToData(&rawFrameNew, &qamDataNew);
  NAVDAT_Modulator_transmit(&Mod, &qamDataNew);

  /* сдвиг данных */
  if (WData.isShift())
  {
    QVector<iqSample_t> buf;
    SizeEx_t n;
    if (WData.isRandShift())
      n = 2*(rand() % sync.syncWindow.SymbolSize) - sync.syncWindow.SymbolSize;
    else
      n = WData.Shift();
    if (n < 0)
      n += sync.syncWindow.SymbolSize;
    buf.resize(n);
    clear_buffer_iq(buf.data(), buf.size());

    noise_awgn_iq(&Awgn, buf.data(), buf.size(), buf.data());
    Scope.append(St, buf.data(), buf.size());

    navdat_mle_sync(&sync, buf.data(), buf.size());
  }
}


void ModulationTest::reset()
{
  NAVDAT_Modulator_reset(&Mod);

  fftModPlot.clear();
  Scope.clear();
  syncCorrealtionPlot.clear();
  syncFreqEstimationPlot.clear();
  syncWindowPlot.clear();
  fftDemPlot.clear();
  equalizerPlot.clear();
  dPhasePlot.clear();
  scatterPlot.clear();

  NAVDAT_mleSync_reset(&sync);
  NAVDAT_Demodulator_reset(&deMod);
}

void ModulationTest::prestart()
{
  reset();

  if (WData.isAutoTransmit())
    transmit();
}

void ModulationTest::test()
{
  clear_buffer_iq(ChannelBuf.data(), ChannelBuf.size());

#if 1
  navdat_modulator_iq(&Mod, ChannelBuf.data(), ChannelBuf.size());
  gain_iq(WData.SiganalGain(), ChannelBuf.data(), ChannelBuf.size(), ChannelBuf.data());
#else
  random_sample_iq(&(ChannelBuf.data()[sync.L]), sync.N);
  gain_iq(0.1, &(ChannelBuf.data()[sync.L]), sync.N - sync.L, &(ChannelBuf.data()[sync.L]));
  copy_buffer_iq(&(ChannelBuf.data()[sync.N]), sync.L, ChannelBuf.data());
#endif

  Noise_Awgn_setLevel_dB(&Awgn, WData.NoiseLevel_dB());
  noise_awgn_iq(&Awgn, ChannelBuf.data(), ChannelBuf.size(), ChannelBuf.data());

  Scope.append(St, ChannelBuf.data(), ChannelBuf.size());

  navdat_mle_sync(&sync, ChannelBuf.data(), ChannelBuf.size());
}
