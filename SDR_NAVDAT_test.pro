QT += core gui
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = SDR_NAVDAT_Test
TEMPLATE = app

include (TestSystem/TestSystem.pri)
include (navdat.pri)

SOURCES += \
    main.cpp \
    ModulationTest.cpp \
    DataWidget.cpp

HEADERS += \
    ModulationTest.h \
    DataWidget.h

FORMS += \
    DataWidget.ui

#DEFINES += NAVDAT_DEBUG_MODULATION_PILOTS_PHASE_CONST

