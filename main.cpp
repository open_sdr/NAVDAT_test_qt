#include <QApplication>
#include "ModulationTest.h"

using namespace SDR;

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    ModulationTest test;
    test.Control()->show();

    return a.exec();
}
